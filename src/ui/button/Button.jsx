import { Component } from 'react';
import s from './Button.module.scss';
import cn from 'classnames';

class Button extends Component {
    render() {
        const { onClick, className, children, text, backgroundColor } = this.props;

        return (
            <button onClick={onClick} className={cn(className, s.button, s[backgroundColor])}>
                {text || children}
            </button>
        );
    }
}

export default Button;
