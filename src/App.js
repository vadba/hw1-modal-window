import { Component } from 'react';
import logo from './logo.svg';
import './App.scss';
import Button from './ui/button';
import Modal from './ui/modal/Modal';

class App extends Component {
    state = {
        openedModal1: false,
        openedModal2: false,
    };

    handleOpenModal1 = () => {
        this.setState({ openedModal1: true });
    };

    handleCloseModal1 = () => {
        this.setState({ openedModal1: false });
    };

    handleOpenModal2 = () => {
        this.setState({ openedModal2: true });
    };

    handleCloseModal2 = () => {
        this.setState({ openedModal2: false });
    };

    render() {
        return (
            <div className='App'>
                <header className='App-header'>
                    <Modal opened={this.state.openedModal1} onClose={this.handleCloseModal1}>
                        <Button className={'buttonfirst'} text='OK' backgroundColor='red' />
                        <Button className={'buttonfirst'} text='Cancel' backgroundColor='red' />
                    </Modal>
                    <Modal
                        opened={this.state.openedModal2}
                        onClose={this.handleCloseModal2}
                        header='Edit Card'
                        closeButton={true}
                        text='Lorem text'
                        backgroundColor='yellow'
                        backgroundColorTitle='green'
                    >
                        <Button className={'buttonfirst'} text='OK' backgroundColor='orange' />
                        <Button className={'buttonfirst'} text='Cancel' backgroundColor='orange' />
                    </Modal>
                    <img src={logo} className='App-logo' alt='logo' />
                    <div className='buttons'>
                        <Button
                            className={'buttonfirst'}
                            onClick={() => {
                                this.handleOpenModal1();
                            }}
                            text='Open first modal'
                            backgroundColor='blue'
                        />
                        <Button
                            className={'buttonsecond'}
                            onClick={() => {
                                this.handleOpenModal2();
                            }}
                            text='Open second modal'
                            backgroundColor='orange'
                        />
                    </div>
                </header>
            </div>
        );
    }
}

export default App;
